package com.mlfl.imasd.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Employee implements Serializable {
    int id;
    String area;
    String nombre;
    @SerializedName("apellido_paterno")
    String apellidoPaterno;
    @SerializedName("apellido_materno")
    String apellidoMaterno;
    String telefono;
    @SerializedName("fecha_nacimiento")
    String fechaNacimiento;
    String email;

    public Employee(){
        super();
    }

    public Employee(int id, String area, String nombre, String apellidoPaterno, String apellidoMaterno, String telefono, String fechaNacimiento, String email){
        super();
        this.id = id;
        this.area = area;
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.telefono = telefono;
        this.fechaNacimiento = fechaNacimiento;
        this.email = email;
    }

    public Employee(String area, String nombre, String apellidoPaterno, String apellidoMaterno, String telefono, String fechaNacimiento, String email ){
        this.area = area;
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.telefono = telefono;
        this.fechaNacimiento = fechaNacimiento;
        this.email = email;
    }

    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public String getArea(){
        return area;
    }

    public void setArea(String area){
        this.area = area;
    }

    public String getNombre(){
        return nombre;
    }

    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public String getApellidoPaterno(){
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno){
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno(){
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno){
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getTelefono(){
        return telefono;
    }

    public void setTelefono(String telefono){
        this.telefono = telefono;
    }

    public String getFechaNacimiento(){
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento){
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getEmail(){
        return  email;
    }

    public void setEmail(String email){
        this.email = email;
    }


    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Employee){
            Employee element = (Employee) obj;
            if(element != null && String.valueOf(this.id).equals(String.valueOf(element.id))
                    && this.area.equals(element.area)
                    && this.nombre.equals(element.nombre)
                    && this.apellidoPaterno.equals(element.apellidoPaterno)
                    && this.apellidoMaterno.equals(element.apellidoMaterno)
                    && this.telefono.equals(element.telefono)
                    && this.fechaNacimiento.equals(element.fechaNacimiento)
                    && this.email.equals(element.email)){
                return true;
            }
        }
        return false;
    }

}
