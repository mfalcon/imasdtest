package com.mlfl.imasd.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.mlfl.imasd.model.Employee;

import java.util.ArrayList;
import java.util.List;

public class EmployeeData extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "employees";
    public static final String TABLE_NAME  = "empleados";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME_AREA = "area";
    public static final String COLUMN_NAME_NOMBRE = "nombre";
    public static final String COLUMN_NAME_APELLIDOPAT = "apellido_paterno";
    public static final String COLUMN_NAME_APELLIDOMAT = "apellido_materno";
    public static final String COLUMN_NAME_TELEFONO ="telefono";
    public static final String COLUMN_MAME_FECHANAC = "fecha_nacimiento";
    public static final String COLUMN_NAME_EMAIL = "email";

    public EmployeeData(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "("
                + COLUMN_ID + " INTEGER PRIMARY KEY,"
                + COLUMN_NAME_AREA + " TEXT,"
                + COLUMN_NAME_NOMBRE + " TEXT,"
                + COLUMN_NAME_APELLIDOPAT + " TEXT,"
                + COLUMN_NAME_APELLIDOMAT + " TEXT,"
                + COLUMN_NAME_TELEFONO + " TEXT,"
                + COLUMN_MAME_FECHANAC + " TEXT,"
                + COLUMN_NAME_EMAIL + " TEXT" + ")";

        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    //Adding new employee
    public void addEmployee(Employee employee){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_AREA, employee.getArea());
        values.put(COLUMN_NAME_NOMBRE, employee.getNombre());
        values.put(COLUMN_NAME_APELLIDOPAT, employee.getApellidoPaterno());
        values.put(COLUMN_NAME_APELLIDOMAT, employee.getApellidoMaterno());
        values.put(COLUMN_NAME_TELEFONO, employee.getTelefono());
        values.put(COLUMN_MAME_FECHANAC, employee.getFechaNacimiento());
        values.put(COLUMN_NAME_EMAIL, employee.getEmail());

        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    //Getting a single employee
    Employee getEmployee(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, new String[]{
                COLUMN_ID,
                COLUMN_NAME_AREA,
                COLUMN_NAME_NOMBRE,
                COLUMN_NAME_APELLIDOPAT,
                COLUMN_NAME_APELLIDOMAT,
                COLUMN_NAME_TELEFONO,
                COLUMN_MAME_FECHANAC,
                COLUMN_NAME_EMAIL
        }, COLUMN_ID + " = ?",
                new String[]{
                        String.valueOf(id)
                }, null, null, null, null);
        if(cursor != null){
            cursor.moveToFirst();
        }

        Employee employee = new Employee(
                Integer.parseInt(cursor.getString(0)),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getString(4),
                cursor.getString(5),
                cursor.getString(6),
                cursor.getString(7)
        );

        return employee;
    }

    //Getting all employees
    public List getAllEmployees(){
        List employeeList = new ArrayList();
        String query = "SELECT * FROM " + TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if(cursor.moveToFirst()){
            do {
                Employee employee = new Employee();
                employee.setId(Integer.parseInt(cursor.getString(0)));
                employee.setArea(cursor.getString(1));
                employee.setNombre(cursor.getString(2));
                employee.setApellidoPaterno(cursor.getString(3));
                employee.setApellidoMaterno(cursor.getString(4));
                employee.setTelefono(cursor.getString(5));
                employee.setFechaNacimiento(cursor.getString(6));
                employee.setEmail(cursor.getString(7));

                employeeList.add(employee);
            } while (cursor.moveToNext());
        }

        return  employeeList;
    }

    //Updating one employee
    public int updateEmployee(Employee employee){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_AREA, employee.getArea());
        values.put(COLUMN_NAME_NOMBRE, employee.getNombre());
        values.put(COLUMN_NAME_APELLIDOPAT, employee.getApellidoPaterno());
        values.put(COLUMN_NAME_APELLIDOMAT, employee.getApellidoMaterno());
        values.put(COLUMN_NAME_TELEFONO, employee.getTelefono());
        values.put(COLUMN_MAME_FECHANAC, employee.getFechaNacimiento());
        values.put(COLUMN_NAME_EMAIL, employee.getEmail());

        return db.update(TABLE_NAME, values, COLUMN_ID + " = ?",
            new String[] {
                    String.valueOf(employee.getId())
        });
    }

    //Deleting one single employee
    public void deleteEmployee(Employee employee){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, COLUMN_ID + " = ?",
                new String[] {
                        String.valueOf(employee.getId())
        });
    }

    //Deleting all employees
    public void deleteAllEmployees(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, null, null);
        db.close();
    }

    //Count all employees
    public int getEmployeesCount(){
        String query = "SELECT * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        cursor.close();

        return  cursor.getCount();
    }
}
