package com.mlfl.imasd.utils;

import android.annotation.SuppressLint;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Constants {
    String url;

    //Poner la url de maquina local o del servidor
    public Constants(){
        url = "http://192.168.1.72:3000/api/v1/";
    }

    public String getUrl(){
        return url;
    }

    //Validar fecha
    public boolean validateDate(String fecha){
        boolean resultado;
        if (fecha.matches("\\d{2}-\\d{2}-\\d{4}")) {
            resultado = true;
        } else {
            resultado = false;
        }

        return resultado;
    }

    //VAlidar numero de teléfono
    public boolean esNumero(String cadena) {

        boolean resultado;

        String regex = "[0-9]+";
        Pattern p = Pattern.compile(regex);
        if (cadena == null) {
            return false;
        }
        Matcher m = p.matcher(cadena);
        return m.matches();
    }

    //Validar email
    public boolean validateEmail(String email){
        boolean valid = false;
        Pattern pattern = Pattern
                .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

        Matcher mather = pattern.matcher(email);

        if (mather.find() == true) {
           valid = true;
        } else {
           valid = false;
        }

        return valid;
    }

    //Obtener edad
    public int getEdad(String dateString) throws ParseException {
        Date date = null;
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        try {
            date = sdf.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(date == null) return 0;

        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.setTime(date);

        int year = dob.get(Calendar.YEAR);
        int month = dob.get(Calendar.MONTH);
        int day = dob.get(Calendar.DAY_OF_MONTH);

        dob.set(year, month+1, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)){
            age--;
        }

        return age;
    }
}
