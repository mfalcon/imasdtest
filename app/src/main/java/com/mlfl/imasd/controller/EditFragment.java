package com.mlfl.imasd.controller;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mlfl.imasd.R;
import com.mlfl.imasd.data.EmployeeData;
import com.mlfl.imasd.model.Employee;
import com.mlfl.imasd.utils.Constants;

import java.text.ParseException;

public class EditFragment extends Fragment {
    EditText txtNombre, txtApellidoPaterno, txtApellidoMaterno, txtFechaNac, txtEmail, txtTelefono;
    TextView txtEdad;
    Spinner txtArea;
    EmployeeData bd;
    Employee employeeData;
    Employee employee;
    Button btnEditar;
    String areasList[] = {"Ninguna", "Software", "Consultoría"};
    String area;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        txtNombre = view.findViewById(R.id.txtNombreEmpleado);
        txtApellidoPaterno = view.findViewById(R.id.txtApellidoPaternoEmpleado);
        txtApellidoMaterno = view.findViewById(R.id.txtApellidoMaternoEmpleado);
        txtArea = view.findViewById(R.id.txtAreaEmpleado);
        txtFechaNac = view.findViewById(R.id.txtFechaNacimientoEmpleado);
        txtEmail = view.findViewById(R.id.txtEmailEmpleado);
        txtTelefono = view.findViewById(R.id.txtTelefonoEmpleado);
        txtEdad = view.findViewById(R.id.txtEdadEmpleado);

        txtArea.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                area = areasList[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ArrayAdapter arrayAdapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_dropdown_item, areasList);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        txtArea.setAdapter(arrayAdapter);

        btnEditar = view.findViewById(R.id.btnGuardar);
        btnEditar.setText("Actualizar");

        disableText();

        Bundle bundle = this.getArguments();
        if(bundle != null){
            employeeData = (Employee) bundle.getSerializable("DataEmployee");
            try {
                setInformation(employeeData);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            btnEditar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Constants objConstants = new Constants();
                    if(txtNombre.getText().length() == 0 || txtApellidoPaterno.getText().length() == 0 || txtApellidoMaterno.getText().length() == 0 || area.equals("Ninguna")|| txtFechaNac.getText().length() == 0 || txtEmail.getText().length() == 0 || txtTelefono.getText().length() == 0) {
                        Toast.makeText(getContext(), "Debe ingresar todos los campos", Toast.LENGTH_LONG).show();
                    } else {
                        String nombre = txtNombre.getText().toString();
                        String apellidoPaterno = txtApellidoPaterno.getText().toString();
                        String apellidoMaterno = txtApellidoMaterno.getText().toString();
                        String telefono = txtTelefono.getText().toString();
                        String fechaNacimiento = txtFechaNac.getText().toString();
                        String email = txtEmail.getText().toString();
                        int id = employeeData.getId();
                        bd = new EmployeeData(getActivity().getBaseContext());

                        boolean valid = false, validphone = false, validate = false;

                        valid = objConstants.validateEmail(email);
                        validphone = objConstants.esNumero(telefono);
                        validate = objConstants.validateDate(fechaNacimiento);


                        if(valid){
                            if((validphone && telefono.length() == 10)){
                                if(validate) {
                                    employee = new Employee(id, area, nombre, apellidoPaterno, apellidoMaterno, telefono, fechaNacimiento, email);
                                    bd.updateEmployee(employee);
                                    Toast.makeText(getContext(), "El empleado ha sido actualizado correctamente", Toast.LENGTH_LONG).show();
                                    disableText();
                                } else {
                                    Toast.makeText(getContext(), "Debe ingresar una fehca con el formato 01-01-1234  valido", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(getContext(), "Debe ingresar un telefono  valido", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(getContext(), "Debe ingresar un email valido", Toast.LENGTH_LONG).show();
                        }


                    }
                }
            });
        }

    }

    public void disableText(){
        txtNombre.setEnabled(false);
        txtApellidoPaterno.setEnabled(false);
        txtApellidoMaterno.setEnabled(false);
        txtTelefono.setEnabled(false);
        txtArea.setEnabled(false);
        txtFechaNac.setEnabled(false);
        txtEmail.setEnabled(false);
        txtEdad.setEnabled(false);
        btnEditar.setEnabled(false);
    }

    public void setInformation(Employee employee) throws ParseException {
        Constants objConstants = new Constants();
        txtNombre.setText(employee.getNombre());
        txtApellidoPaterno.setText(employee.getApellidoPaterno());
        txtApellidoMaterno.setText(employee.getApellidoMaterno());
        txtTelefono.setText(employee.getTelefono());
        String areaFrom = employee.getArea();
        switch(areaFrom){
            case "Software":
                txtArea.setSelection(1);
                break;
            case "Consultoría":
                txtArea.setSelection(2);
                break;
            default:
                txtArea.setSelection(0);
                break;
        }
        txtFechaNac.setText(employee.getFechaNacimiento());
        int edad = objConstants.getEdad(employee.getFechaNacimiento());
        txtEdad.setText(String.valueOf(edad));
        txtEmail.setText(employee.getEmail());

    }

    public void enableText(){
        txtNombre.setEnabled(true);
        txtApellidoPaterno.setEnabled(true);
        txtApellidoMaterno.setEnabled(true);
        txtTelefono.setEnabled(true);
        txtArea.setEnabled(true);
        txtFechaNac.setEnabled(true);
        txtEmail.setEnabled(true);
        btnEditar.setEnabled(true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu items for use in the action bar
        inflater.inflate(R.menu.menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_edit:
                enableText();
                return false;
            default:
                break;
        }

        return false;
    }



}