package com.mlfl.imasd.controller;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.mlfl.imasd.R;
import com.mlfl.imasd.data.EmployeeData;
import com.mlfl.imasd.model.Employee;
import com.mlfl.imasd.utils.Constants;

public class AddFragment extends Fragment {
    EditText txtNombre, txtApellidoPaterno, txtApellidoMaterno, txtFechaNacimiento, txtEmail, txtTelefono;
    Spinner txtArea;
    Button btnGuardar;
    EmployeeData bd;
    Employee employee;
    String areasList[] = {"Ninguna", "Software", "Consultoría"};
    String area;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        txtNombre = view.findViewById(R.id.txtNombreEmpleado);
        txtApellidoPaterno = view.findViewById(R.id.txtApellidoPaternoEmpleado);
        txtApellidoMaterno = view.findViewById(R.id.txtApellidoMaternoEmpleado);
        txtArea = view.findViewById(R.id.txtAreaEmpleado);
        txtFechaNacimiento = view.findViewById(R.id.txtFechaNacimientoEmpleado);
        txtEmail = view.findViewById(R.id.txtEmailEmpleado);
        txtTelefono = view.findViewById(R.id.txtTelefonoEmpleado);
        btnGuardar = view.findViewById(R.id.btnGuardar);

        txtArea.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                area = areasList[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ArrayAdapter arrayAdapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_dropdown_item, areasList);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        txtArea.setAdapter(arrayAdapter);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtNombre.getText().length() == 0 || txtApellidoPaterno.getText().length() == 0 ||  area.equals("Ninguna") ||txtApellidoMaterno.getText().length() == 0 || txtFechaNacimiento.getText().length() == 0 || txtEmail.getText().length() == 0 || txtTelefono.getText().length() == 0) {
                    Toast.makeText(getContext(), "Debe ingresar todos los campos", Toast.LENGTH_LONG).show();
                } else {
                    Constants objConstants = new Constants();
                    String nombre = txtNombre.getText().toString();
                    String apellidoPaterno = txtApellidoPaterno.getText().toString();
                    String apellidoMaterno = txtApellidoMaterno.getText().toString();
                    String fechaNacimiento = txtFechaNacimiento.getText().toString();
                    String telefono = txtTelefono.getText().toString();
                    String email = txtEmail.getText().toString();

                    boolean valid = false, validphone = false, validate = false;

                    valid = objConstants.validateEmail(email);
                    validphone = objConstants.esNumero(telefono);
                    validate = objConstants.validateDate(fechaNacimiento);


                    if(valid){
                        if((validphone && telefono.length() == 10)){
                            if(validate) {
                                bd = new EmployeeData(getActivity().getBaseContext());
                                employee = new Employee(area, nombre, apellidoPaterno, apellidoMaterno, telefono, fechaNacimiento, email);
                                bd.addEmployee(employee);
                                Toast.makeText(getContext(), "El empleado ha sido ingresado correctamente", Toast.LENGTH_LONG).show();
                                clear();
                            } else {
                                Toast.makeText(getContext(), "Debe ingresar una fehca con el formato 01-01-1234  valido", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(getContext(), "Debe ingresar un telefono  valido", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(getContext(), "Debe ingresar un email valido", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

    }

    public void clear(){
        txtEmail.setText("");
        txtFechaNacimiento.setText("");
        txtApellidoMaterno.setText("");
        txtApellidoPaterno.setText("");
        txtNombre.setText("");
        txtTelefono.setText("");
        txtArea.setSelection(0);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

}