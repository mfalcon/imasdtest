package com.mlfl.imasd.controller.adapter;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.mlfl.imasd.R;
import com.mlfl.imasd.controller.AddFragment;
import com.mlfl.imasd.controller.EditFragment;
import com.mlfl.imasd.data.EmployeeData;
import com.mlfl.imasd.model.Employee;
import com.mlfl.imasd.network.ImasdAppApiAdapter;
import com.mlfl.imasd.utils.Constants;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainFragment extends Fragment {

    ArrayList<Employee> employeesList;
    ArrayList<Employee> employeeListWS;
    EmployeeData db;
    Button btnAgregar;
    ListView listView;
    EmployeeListAdapter employeeListAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listView = view.findViewById(R.id.lvEmpleado);
        btnAgregar = view.findViewById(R.id.btnAgregar);

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    AddFragment addFragment = new AddFragment();
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container, addFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        getAllEmployees();

        employeeListAdapter = new EmployeeListAdapter(getActivity(), employeesList, db);

        listView.setAdapter(employeeListAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                EditFragment editFragment = new EditFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("DataEmployee", employeesList.get(position));
                editFragment.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container, editFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

    }

    public  void getAllEmployees(){
        employeesList = new ArrayList<>();
        db = new EmployeeData(getActivity().getApplicationContext());
        employeesList = (ArrayList) db.getAllEmployees();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach(){
        super.onDetach();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu items for use in the action bar
        inflater.inflate(R.menu.menu_principal, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_sync:
                employeeListWS = new ArrayList<>();
                Constants objConstants = new Constants();
                Call<ArrayList<Employee>> employeeCall = ImasdAppApiAdapter.getApiService().getEmpleados();
                employeeCall.enqueue(new Callback<ArrayList<Employee>>() {
                    @Override
                    public void onResponse(@NotNull Call<ArrayList<Employee>> employeeCall, @NotNull Response<ArrayList<Employee>> response) {
                        if(response.isSuccessful()){
                            employeeListWS = response.body();
                            if(employeeListWS.size() != 0){

                                for(Employee emp: employeesList) {
                                    if(employeeListWS.contains(emp)){
                                        String url = objConstants.getUrl() + "empleados/" + emp.getId() + "/";
                                        Call<Void> updateEmployee = ImasdAppApiAdapter.getApiService().patchEmpleado(url, emp);
                                        updateEmployee.enqueue(new Callback<Void>() {
                                            @Override
                                            public void onResponse(@NotNull Call<Void> updateEmployee, @NotNull Response<Void> response) {
                                                if(response.isSuccessful()){
                                                    System.out.println("ACTUALIZADOS");
                                                } else {
                                                    System.out.println("NOT ACTUALIZADOS");
                                                }
                                            }

                                            @Override
                                            public void onFailure(@NotNull Call<Void> updateEmployee, @NotNull Throwable t) {

                                            }
                                        });
                                    } else {
                                        Call<Void> postEmployee = ImasdAppApiAdapter.getApiService().postEmpleado(emp);
                                        postEmployee.enqueue(new Callback<Void>() {
                                            @Override
                                            public void onResponse(@NotNull Call<Void> postEmployee, @NotNull Response<Void> response) {
                                                if(response.isSuccessful()){
                                                    System.out.println("INSERTADOS");
                                                } else {
                                                    System.out.println("NO INSERTADOS");
                                                }

                                            }
                                            @Override
                                            public void onFailure(@NotNull Call<Void> postEmployee, @NotNull Throwable t) {

                                            }
                                        });
                                    }
                                }
                            } else {
                                for (Employee elementSQLITE : employeesList){
                                    Call<Void> postEmployee = ImasdAppApiAdapter.getApiService().postEmpleado(elementSQLITE);
                                    postEmployee.enqueue(new Callback<Void>() {
                                        @Override
                                        public void onResponse(@NotNull Call<Void> postEmployee, @NotNull Response<Void> response) {
                                            if(response.isSuccessful()){
                                                System.out.println("INSERTADOS TODOS");
                                            }

                                        }

                                        @Override
                                        public void onFailure(@NotNull Call<Void> postEmployee, @NotNull Throwable t) {

                                        }
                                    });
                                }

                            }
                            Toast.makeText(getContext(), "Datos Sincronizados Correctamente", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<ArrayList<Employee>> employeeCall, @NotNull Throwable t) {

                    }
                });

                for(int i=0; i<employeeListWS.size(); i++){
                    System.out.println("TEST: " + employeeListWS.get(i).getNombre());
                }

                return false;
            default:
                break;
        }

        return false;
    }
}