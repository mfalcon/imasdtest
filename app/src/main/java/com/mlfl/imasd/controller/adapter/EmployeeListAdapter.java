package com.mlfl.imasd.controller.adapter;

import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.mlfl.imasd.R;
import com.mlfl.imasd.data.EmployeeData;
import com.mlfl.imasd.model.Employee;

import java.util.ArrayList;

public class EmployeeListAdapter  extends BaseAdapter {
    private Activity context;
    ArrayList<Employee> employes;
    EmployeeData db;
    BaseAdapter baseAdapter;

    public EmployeeListAdapter(Activity context, ArrayList<Employee> employes, EmployeeData db){
        this.context = context;
        this.db = db;
        this.employes = employes;
    }

    public static class ViewHolder{
        TextView txtNombre, txtArea;
        Button btnDelete;

    }

    @Override
    public int getCount() {
        return employes.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        LayoutInflater inflater = context.getLayoutInflater();
        ViewHolder vh;
        if(convertView == null){
            vh = new ViewHolder();
            row = inflater.inflate(R.layout.row_employee, null, true);
            vh.txtNombre = row.findViewById(R.id.txtNombre);
            vh.txtArea = row.findViewById(R.id.txtarea);
            vh.btnDelete = row.findViewById(R.id.btnDelete);

            row.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }

        vh.txtNombre.setText(employes.get(position).getNombre() + " " + employes.get(position).getApellidoPaterno() + " " + employes.get(position).getApellidoMaterno());
        vh.txtArea.setText(employes.get(position).getArea());

        vh.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.deleteEmployee(employes.get(position));
                Toast.makeText(context, "El empleado ha sido eliminado correctamente", Toast.LENGTH_LONG).show();
                employes = (ArrayList) db.getAllEmployees();
                notifyDataSetChanged();
            }
        });

        return row;
    }
}
