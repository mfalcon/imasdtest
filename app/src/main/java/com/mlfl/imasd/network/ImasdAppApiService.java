package com.mlfl.imasd.network;

import com.mlfl.imasd.model.Employee;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface ImasdAppApiService {

    //Get All Employees
    @GET("empleados/")
    Call<ArrayList<Employee>> getEmpleados();

    //Get One Employee
    @GET
    Call<Employee> getEmpleado(@Url String url);

    //Post Employee
    @POST("empleados/")
    Call<Void> postEmpleado(@Body Employee employee);

    //Update Employee
    @PATCH
    Call<Void> patchEmpleado(@Url String url, @Body Employee employee);


}
